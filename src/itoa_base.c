/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   itoa_base.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 15:27:41 by jwong             #+#    #+#             */
/*   Updated: 2018/04/10 10:56:04 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "../libft/includes/libft.h"

static void			ft_storenb(size_t nb, char *base, int *i, char *s)
{
	int len;

	len = ft_strlen(base);
	if ((nb / len) == 0)
	{
		s[*i] = base[(nb % len)];
		(*i)++;
		s[*i] = '\0';
	}
	else
	{
		ft_storenb((nb / len), base, i, s);
		s[*i] = base[(nb % len)];
		(*i)++;
		s[*i] = '\0';
	}
}

void				itoa_base(size_t nb, char *base, int opt)
{
	int		i;
	char	ret[20];

	if (opt == 1)
	{
		ret[0] = '0';
		ret[1] = 'x';
		i = 2;
	}
	else
		i = 0;
	ft_storenb(nb, base, &i, ret);
	ft_putstr(ret);
}
