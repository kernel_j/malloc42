/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 11:32:46 by jwong             #+#    #+#             */
/*   Updated: 2018/04/10 10:56:34 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/includes/libft.h"
#include "malloc.h"

bool	check_size(t_header *header, size_t size)
{
	int		zone;
	int		request_zone;
	t_meta	*meta;

	zone = get_zone(header->size);
	request_zone = get_zone(size);
	if (zone == ZONE_LARGE && request_zone == ZONE_LARGE)
	{
		meta = header->meta;
		if ((meta->mmap_size - sizeof(t_meta) - sizeof(t_header)) < size)
			return (true);
		return (false);
	}
	else if (zone == request_zone)
		return (false);
	return (true);
}

void	*realloc(void *ptr, size_t size)
{
	t_header	*header;
	bool		change;
	void		*p;

	if (!ptr)
		return (malloc(size));
	if (ptr && size == 0)
	{
		free(ptr);
		return (NULL);
	}
	header = (t_header *)(ptr - sizeof(t_header));
	if (header->magic_num != MAGIC_NUM)
		return (NULL);
	change = check_size(header, size);
	if (change == true)
	{
		p = malloc(size);
		ft_memcpy(p, ptr, header->size);
		free(ptr);
		return (p);
	}
	else
		header->size = size;
	return (ptr);
}
