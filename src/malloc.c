/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 11:38:21 by jwong             #+#    #+#             */
/*   Updated: 2018/04/10 11:15:31 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "../libft/includes/libft.h"
#include "malloc.h"

t_heap_data g_malloc_heap;
pthread_mutex_t g_mutex = PTHREAD_MUTEX_INITIALIZER;

bool	is_heap_initialized(int zone)
{
	if (zone == ZONE_TINY && g_malloc_heap.tiny)
		return (true);
	else if (zone == ZONE_SMALL && g_malloc_heap.small)
		return (true);
	else if (zone == ZONE_LARGE && g_malloc_heap.large)
		return (true);
	else
		return (false);
}

void	init_heap(int zone, size_t size)
{
	t_meta	*ptr;

	ptr = create_memory(zone, size);
	if (!ptr)
		return ;
	if (zone == ZONE_TINY)
		g_malloc_heap.tiny = ptr;
	else if (zone == ZONE_SMALL)
		g_malloc_heap.small = ptr;
	else if (zone == ZONE_LARGE)
		g_malloc_heap.large = ptr;
}

void	*search_heap(int zone, size_t size)
{
	t_meta	*ptr;
	void	*ret;

	ptr = NULL;
	if (zone == ZONE_TINY)
		ptr = g_malloc_heap.tiny;
	else if (zone == ZONE_SMALL)
		ptr = g_malloc_heap.small;
	else if (zone == ZONE_LARGE)
		ptr = g_malloc_heap.large;
	while (ptr)
	{
		if (ptr->free_blocks > 0)
		{
			ret = find_free_block(ptr->first_block, size);
			if (ret)
			{
				ptr->free_blocks -= 1;
				return (ret);
			}
		}
		ptr = ptr->next_meta;
	}
	return (NULL);
}

void	add_memory_to_heap(int zone, size_t size)
{
	t_meta	*ptr;
	t_meta	**head;

	head = NULL;
	ptr = (t_meta *)create_memory(zone, size);
	if (zone == ZONE_TINY)
		head = &g_malloc_heap.tiny;
	else if (zone == ZONE_SMALL)
		head = &g_malloc_heap.small;
	else if (zone == ZONE_LARGE)
		head = &g_malloc_heap.large;
	attach_to_heap(head, ptr);
}

void	*malloc(size_t size)
{
	int		zone;
	void	*ptr;

	pthread_mutex_lock(&g_mutex);
	zone = get_zone(size);
	if (!is_heap_initialized(zone))
		init_heap(zone, size);
	ptr = search_heap(zone, size);
	if (!ptr)
	{
		add_memory_to_heap(zone, size);
		ptr = search_heap(zone, size);
	}
	pthread_mutex_unlock(&g_mutex);
	return (ptr);
}
