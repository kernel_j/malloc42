/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 11:46:23 by jwong             #+#    #+#             */
/*   Updated: 2018/04/10 10:56:55 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "malloc.h"

int		get_zone(size_t size)
{
	if (size <= MALLOC_TINY)
		return (ZONE_TINY);
	else if (size <= MALLOC_SMALL)
		return (ZONE_SMALL);
	else
		return (ZONE_LARGE);
}

size_t	get_mmap_size(size_t size)
{
	size_t	len;
	size_t	page;

	len = 0;
	page = getpagesize();
	if (size <= MALLOC_TINY)
		len = MIN_BLOCKS * (MALLOC_TINY + sizeof(t_header)) + sizeof(t_meta);
	else if (size > MALLOC_TINY && size <= MALLOC_SMALL)
		len = MIN_BLOCKS * (MALLOC_SMALL + sizeof(t_header)) + sizeof(t_meta);
	else if (size > MALLOC_SMALL)
		len = size + sizeof(t_header) + sizeof(t_meta);
	if (len % page != 0)
		len = (len / page) * page + page;
	else
		len = (len / page) * page;
	return (len);
}

void	*find_free_block(void *first_block, size_t size)
{
	t_header	*ptr;

	ptr = (t_header *)first_block;
	while (ptr)
	{
		if (ptr->free == true)
		{
			ptr->size = size;
			ptr->free = false;
			return ((void *)ptr + sizeof(t_header));
		}
		ptr = ptr->next;
	}
	return (NULL);
}

bool	addr_in_heap(void *ptr, t_meta *heap)
{
	t_meta	*tmp;

	tmp = heap;
	while (tmp)
	{
		if ((size_t)ptr > (size_t)tmp
				&& (size_t)ptr < (size_t)((void *)tmp + tmp->mmap_size))
			return (true);
		tmp = tmp->next_meta;
	}
	return (false);
}
