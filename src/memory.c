/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memory.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 11:34:03 by jwong             #+#    #+#             */
/*   Updated: 2018/04/10 10:56:20 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/mman.h>
#include "../libft/includes/libft.h"
#include "malloc.h"

void	init_meta_data(void *ptr, size_t mmap_size)
{
	t_meta	meta;

	ft_bzero(&meta, sizeof(meta));
	meta.mmap_size = mmap_size;
	meta.magic_num = MAGIC_NUM;
	meta.first_block = ptr + sizeof(t_meta);
	ft_memcpy(ptr, &meta, sizeof(meta));
}

void	init_header(t_header *ptr, t_meta *meta)
{
	void		*pos;
	t_header	header;

	pos = (void *)ptr;
	ft_bzero(&header, sizeof(header));
	header.free = true;
	header.magic_num = MAGIC_NUM;
	header.meta = meta;
	ft_memcpy(pos, &header, sizeof(header));
}

size_t	create_blocks(void *ptr, t_meta *meta,
		size_t mem_len, size_t block_size)
{
	size_t		i;
	size_t		blocks;
	t_header	*pos;

	pos = (t_header *)(ptr + sizeof(t_meta));
	i = block_size;
	blocks = 0;
	while (i < mem_len)
	{
		init_header(pos, meta);
		if ((i + block_size) < mem_len)
			pos->next = (t_header *)((void *)pos + block_size);
		if (i != block_size)
			pos->prev = (t_header *)((void *)pos - block_size);
		pos = (t_header *)((void *)pos + block_size);
		i += block_size;
		blocks++;
	}
	return (blocks);
}

void	split_memory(void *ptr, int zone, size_t mem_len)
{
	size_t		block_size;
	size_t		blocks;
	t_meta		*meta;
	t_header	*pos;

	block_size = 0;
	meta = (t_meta *)ptr;
	pos = (t_header *)(ptr + sizeof(t_meta));
	if (zone == ZONE_LARGE)
	{
		meta->total_blocks = 1;
		meta->free_blocks = 1;
		init_header(pos, meta);
		return ;
	}
	else if (zone == ZONE_SMALL)
		block_size = MALLOC_SMALL + sizeof(t_header);
	else if (zone == ZONE_TINY)
		block_size = MALLOC_TINY + sizeof(t_header);
	blocks = create_blocks(ptr, meta, mem_len, block_size);
	meta->total_blocks = blocks;
	meta->free_blocks = blocks;
}

void	*create_memory(int zone, size_t size)
{
	size_t	len;
	void	*ptr;

	len = get_mmap_size(size);
	ptr = mmap(0, len,
			PROT_READ | PROT_WRITE,
			MAP_SHARED | MAP_ANONYMOUS,
			-1, 0);
	if (ptr == MAP_FAILED)
		return (NULL);
	init_meta_data(ptr, len);
	split_memory(ptr, zone, (len - sizeof(t_meta)));
	return (ptr);
}
