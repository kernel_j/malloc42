/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 11:36:27 by jwong             #+#    #+#             */
/*   Updated: 2018/04/10 10:55:37 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/mman.h>
#include "../libft/includes/libft.h"
#include "malloc.h"

extern	t_heap_data g_malloc_heap;
extern	pthread_mutex_t g_mutex;

int		is_mallocd(void *ptr)
{
	bool	ret;

	ret = addr_in_heap(ptr, g_malloc_heap.tiny);
	if (ret == true)
		return (ZONE_TINY);
	ret = addr_in_heap(ptr, g_malloc_heap.small);
	if (ret == true)
		return (ZONE_SMALL);
	ret = addr_in_heap(ptr, g_malloc_heap.large);
	if (ret == true)
		return (ZONE_LARGE);
	return (-1);
}

void	reattach_meta(t_meta *head, int zone)
{
	t_meta	**base;

	base = NULL;
	if (zone == ZONE_TINY)
		base = &g_malloc_heap.tiny;
	else if (zone == ZONE_SMALL)
		base = &g_malloc_heap.small;
	else if (zone == ZONE_LARGE)
		base = &g_malloc_heap.large;
	if (!head->prev_meta && !head->next_meta)
		*base = NULL;
	else if (!head->prev_meta && head->next_meta)
	{
		*base = head->next_meta;
		(head->next_meta)->prev_meta = NULL;
	}
	else if (head->prev_meta && !head->next_meta)
		(head->prev_meta)->next_meta = NULL;
	else if (head->prev_meta && head->next_meta)
	{
		(head->prev_meta)->next_meta = head->next_meta;
		(head->next_meta)->prev_meta = head->prev_meta;
	}
}

void	munmap_memory(t_meta *head, int zone)
{
	reattach_meta(head, zone);
	munmap((void *)head, head->mmap_size);
}

t_meta	*update_meta_data(void *ptr)
{
	t_header	*header;
	t_meta		*meta;

	header = (t_header *)(ptr - sizeof(t_header));
	if (header->magic_num != MAGIC_NUM)
		return (NULL);
	header->free = true;
	meta = header->meta;
	if (meta->magic_num != MAGIC_NUM)
		return (NULL);
	meta->free_blocks += 1;
	return (meta);
}

void	free(void *ptr)
{
	int		zone;
	t_meta	*meta;

	if (!ptr)
		return ;
	pthread_mutex_lock(&g_mutex);
	zone = is_mallocd(ptr);
	if (zone == -1)
	{
		pthread_mutex_unlock(&g_mutex);
		return ;
	}
	meta = update_meta_data(ptr);
	if (!meta)
	{
		pthread_mutex_unlock(&g_mutex);
		return ;
	}
	if (meta->total_blocks == meta->free_blocks)
		munmap_memory(meta, zone);
	pthread_mutex_unlock(&g_mutex);
}
