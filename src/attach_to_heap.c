/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   attach_to_heap.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 11:29:46 by jwong             #+#    #+#             */
/*   Updated: 2018/04/04 11:31:33 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void	prepend_to_root(t_meta **head, t_meta *ptr)
{
	t_meta	*tmp;

	tmp = *head;
	*head = ptr;
	ptr->next_meta = tmp;
	tmp->prev_meta = ptr;
}

t_meta	*insert_btw_nodes(t_meta **head, t_meta *ptr)
{
	t_meta	*tmp;
	t_meta	*base;

	base = *head;
	while (base->next_meta)
	{
		if ((size_t)base < (size_t)ptr && (size_t)base->next_meta > (size_t)ptr)
		{
			tmp = base->next_meta;
			base->next_meta = ptr;
			ptr->next_meta = tmp;
			ptr->prev_meta = base;
			tmp->prev_meta = ptr;
			return (NULL);
		}
		base = base->next_meta;
	}
	return (base);
}

void	attach_to_heap(t_meta **head, t_meta *ptr)
{
	t_meta	*tmp;

	if ((size_t)*head > (size_t)ptr)
	{
		prepend_to_root(head, ptr);
		return ;
	}
	tmp = insert_btw_nodes(head, ptr);
	if (tmp != NULL)
	{
		tmp->next_meta = ptr;
		ptr->prev_meta = tmp;
	}
}
