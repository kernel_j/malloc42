/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_alloc_mem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 15:31:42 by jwong             #+#    #+#             */
/*   Updated: 2018/04/10 10:56:45 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/includes/libft.h"
#include "malloc.h"

extern t_heap_data g_malloc_heap;
extern pthread_mutex_t g_mutex;

void		print_total(size_t total)
{
	ft_putstr("Total : ");
	ft_putnbr(total);
	ft_putstr(" octets\n");
}

void		header_iter(t_header *header, size_t *total)
{
	t_header	*tmp;

	tmp = header;
	while (tmp)
	{
		if (tmp->free == false)
		{
			itoa_base((size_t)((void *)tmp + sizeof(t_header)), BASE, 1);
			ft_putstr(" - ");
			itoa_base((size_t)((void *)tmp + sizeof(t_header) + tmp->size),
					BASE, 1);
			ft_putstr(" : ");
			ft_putnbr(tmp->size);
			ft_putstr(" octets\n");
			*total += tmp->size;
		}
		tmp = tmp->next;
	}
}

void		meta_iter(t_meta *meta, size_t *total)
{
	t_meta	*tmp;

	tmp = meta;
	while (tmp)
	{
		header_iter((t_header *)tmp->first_block, total);
		tmp = tmp->next_meta;
	}
}

void		print_memory(size_t *total)
{
	if (g_malloc_heap.tiny)
	{
		ft_putstr("TINY : ");
		itoa_base((size_t)g_malloc_heap.tiny, BASE, 1);
		ft_putstr("\n");
		meta_iter(g_malloc_heap.tiny, total);
	}
	if (g_malloc_heap.small)
	{
		ft_putstr("SMALL : ");
		itoa_base((size_t)g_malloc_heap.small, BASE, 1);
		ft_putstr("\n");
		meta_iter(g_malloc_heap.small, total);
	}
	if (g_malloc_heap.large)
	{
		ft_putstr("LARGE : ");
		itoa_base((size_t)g_malloc_heap.large, BASE, 1);
		ft_putstr("\n");
		meta_iter(g_malloc_heap.large, total);
	}
}

void		show_alloc_mem(void)
{
	size_t	total;

	pthread_mutex_lock(&g_mutex);
	total = 0;
	print_memory(&total);
	print_total(total);
	pthread_mutex_unlock(&g_mutex);
}
