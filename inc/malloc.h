/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/12 12:55:42 by jwong             #+#    #+#             */
/*   Updated: 2018/04/10 11:15:10 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef H_MALLOC_H
# define H_MALLOC_H

# include <stdlib.h>
# include <stdbool.h>
# include <pthread.h>

enum	e_zone
{
	ZONE_TINY,
	ZONE_SMALL,
	ZONE_LARGE
};

typedef	struct		s_meta
{
	size_t			total_blocks;
	size_t			free_blocks;
	size_t			mmap_size;
	int				magic_num;
	void			*first_block;
	struct s_meta	*next_meta;
	struct s_meta	*prev_meta;
}					t_meta;

typedef struct		s_header
{
	size_t			size;
	int				magic_num;
	bool			free;
	struct s_header	*next;
	struct s_header	*prev;
	t_meta			*meta;
}					t_header;

typedef struct		s_heap_data
{
	t_meta			*tiny;
	t_meta			*small;
	t_meta			*large;
	bool			initialized;
}					t_heap_data;

# define MALLOC_TINY 64
# define MALLOC_SMALL 1024
# define MIN_BLOCKS 100
# define BASE "0123456789ABCDEF"
# define MAGIC_NUM 0x12345678

/*
** free.c
*/
void				free(void *ptr);

/*
** malloc.c
*/
void				*malloc(size_t size);

/*
** utils.c
*/
int					get_zone(size_t zone);
size_t				get_mmap_size(size_t size);
void				*find_free_block(void *first_block, size_t size);
bool				addr_in_heap(void *ptr, t_meta *heap);

/*
** memory.c
*/
void				*create_memory(int zone, size_t size);

/*
** attach_to_heap.c
*/
void				attach_to_heap(t_meta **head, t_meta *ptr);

/*
** realloc.c
*/
void				*realloc(void *ptr, size_t size);

/*
** show_alloc_mem.c
*/
void				show_alloc_mem(void);

/*
** itoa_base.c
*/
void				itoa_base(size_t nb, char *base, int opt);

#endif
