# README #
This project was done with the goal to rewrite:

* malloc
* free
* realloc

from the standard library

An additional function show_alloc_mem prints the location of all allocated
memory.

### Install ###
Compile the shared library using the rules of the Makefile in the command line.
```
make
```

To re-compile the project
```
make re
```

### Usage ###
To use the library, run either the script run.sh or run_linux.sh (depending on
your current operating system) and a program of choice.

For Mac OS
```
./run.sh ls
```

For Linux
```
./run_linux.sh ls
```

### Unintsall ###
To remove all object files
```
make clean
```

To remove all object file and the shared library
```
make fclean
```

### Testing ###
To test the basic functionality of the shared library, go to the test folder
and compile the c files
```
clang -o test0 test.c
```

To run the test, simply use the run scripts with the test executables
```
./run.sh test/test0
```
