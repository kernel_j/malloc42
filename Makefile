# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jwong <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/10 10:51:46 by jwong             #+#    #+#              #
#    Updated: 2018/04/10 11:41:44 by jwong            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

NAME	= libft_malloc_$(HOSTTYPE).so

SRC		= malloc.c 				\
		  utils.c				\
		  memory.c				\
		  attach_to_heap.c		\
		  itoa_base.c           \
		  free.c				\
		  realloc.c				\
		  show_alloc_mem.c

CC		= clang
CFLAGS	= -Wall -Werror -Wextra -fPIC -I libft/includes/ -I inc/
DIR		= src/
SRCS	= $(addprefix $(DIR), $(SRC))
OBJ		= $(SRCS:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
		make -C libft/
		$(CC) -shared -o $(NAME) $(OBJ) -L libft/ -lft -lpthread
		[ -e libft_malloc.so ] || ln -s $(NAME) libft_malloc.so

%.o: %.c
		$(CC) $(CFLAGS) $< -c -o $@

clean:
		make -C libft/ clean
		rm -f $(OBJ)

fclean: clean
		make -C libft/ fclean
		rm -f $(NAME) libft_malloc.so

re: fclean all
