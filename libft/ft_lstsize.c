/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstsize.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/19 11:25:44 by jwong             #+#    #+#             */
/*   Updated: 2015/12/19 11:26:04 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_lstsize(t_list *lst)
{
	t_list	*tmp;
	size_t	size;

	size = 0;
	if (lst != NULL)
	{
		tmp = lst;
		while (tmp != NULL)
		{
			tmp = (*tmp).next;
			size++;
		}
	}
	return (size);
}
