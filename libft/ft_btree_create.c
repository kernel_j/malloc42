/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btree_create.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/19 11:22:53 by jwong             #+#    #+#             */
/*   Updated: 2015/12/19 11:23:20 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

t_btree	*ft_btree_create(void *item)
{
	t_btree	*new;

	new = (t_btree *)malloc(sizeof(*new));
	if (new != NULL)
	{
		(*new).left = NULL;
		(*new).right = NULL;
		(*new).item = item;
	}
	return (new);
}
